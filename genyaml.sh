#!/usr/bin/env bash


makeyaml () {
    local file=$1
    local type=$2

    declare -A content

    while IFS="=" read -r key value; do content["$key"]=$value; done < <(
        type=$type ./yq '.[env(type)] | to_entries | map([.key, .value] | join("=")) | .[]' $file
    )

    for key in "${!content[@]}"; do
        if [ -z "${content[$key]}" ]
        then
            tag=$key
        else
            tag="$key:${content[$key]}"
        fi
        echo "---" > "out/${type}.${key}.yaml"
        ./clusterctl generate provider --raw "--$type" $tag >> "out/${type}.${key}.yaml";
    done
}

mkdir -p out
makeyaml providers.yaml core
makeyaml providers.yaml bootstrap
makeyaml providers.yaml control-plane
makeyaml providers.yaml infrastructure